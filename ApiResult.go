package apiResult

import (
	"encoding/json"
	"fmt"
	"math"
)

type (
	Result struct {
		HTTPCode int         `json:"-"`
		Succeed  bool        `json:"succeed"`
		Code     int         `json:"code, omitempty"`
		Message  string      `json:"message, omitempty"`
		Dat      interface{} `json:"data,omitempty"`
		Page     *Page       `json:"page,omitempty"`
	}

	Page struct {
		TotalRecords int `json:"totalRecords"` //总记录数量
		TotalPages   int `json:"totalPages"`   //总页数
		PageNo       int `json:"pageNo"`       //当前页号
		PageSize     int `json:"pageSize"`     //每页数据条数
	}

	Errors struct {
		Msg  string
		Code int
	}

	validationError struct {
		ActualTag string `json:"tag"`
		Namespace string `json:"namespace"`
		Kind      string `json:"kind"`
		Type      string `json:"type"`
		Value     string `json:"value"`
		Param     string `json:"param"`
	}
)

var (
	// ECode_Insufficient_User_Permissions Errors = Errors{"xxxxx", 300}
	ECode_Connection_Timeout Errors = Errors{"connection timed out", 5001}
)

// var p1 = Errors{"lisi", 30} //方式A
//ApiErrorCode.Insufficient_User_Permissions
//ErrCode_BodyNotAllowed = &Errors{"xxxxx", 300}

// 默认值
func DefaultInt(def int, val ...int) int {
	if len(val) == 0 {
		return def
	}
	return val[0]
}

func New(success bool, dat interface{}, msg string, code ...int) Result {
	HTTPCode := 200
	if !success {
		HTTPCode = 500
	}

	r := Result{
		Succeed:  success,
		Message:  msg,
		Code:     DefaultInt(0, code...),
		HTTPCode: HTTPCode,
		Dat:      dat,
	}

	return r
}

func Success(dat interface{}, msg ...string) Result {
	m := ""
	if len(msg) > 0 {
		m = msg[0]
	}
	return New(true, dat, m)
}

func Fail(dat interface{}, code ...int) Result {
	return New(false, dat, "", code...)
}

func Message(msg string, code ...int) Result {
	success := true
	if len(code) > 0 && code[0] != 200 {
		success = false
	}
	return New(success, nil, msg, code...)
}

func (r Result) SetPage(page *Page) Result {
	if page.PageSize == 0 {
		r.Page = nil
		return r
	}
	r.Page = page
	r.Page.TotalPages = int(math.Ceil(float64(r.Page.TotalRecords) / float64(r.Page.PageSize)))
	return r
}

func (r Result) JSON() string {
	b, err := json.Marshal(&r)
	if err != nil {
		fmt.Println("生成json字符串错误")
	}
	return string(b)
}

// func (r Result) JSON(ctx iris.Context) {
// 	ctx.StatusCode(r.Code)
// 	ctx.JSON(r)
// }
